<?php

/**
 * @file
 *   Commerce Subscription migration.
 */

class CommerceMigrateUbercartSubscription extends Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);
    $this->orderSourceMigration = 'CommerceMigrateUbercartOrder';
    $this->productSourceMigration = 'CommerceMigrateUbercartNodeProduct';

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'rfid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Ubercart rfid from {uc_recurring_users}',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_recurring')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_ubercart_get_source_connection();

    $query = $connection->select('uc_recurring_users', 'uru');
    $query->innerJoin('uc_orders', 'uo', 'uru.order_id = uo.order_id');
    $query->condition('uo.order_status', 'in_checkout', '<>');
    $query->fields('uru');
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_recurring', 'product');

    // Default uid to 0 if we're not mapping it.
    if (variable_get('commerce_migrate_ubercart_user_map_ok', FALSE) && variable_get('commerce_migrate_ubercart_user_migration_class', "") != "") {
      $this->addFieldMapping('uid', 'uid')->sourceMigration(variable_get('commerce_migrate_ubercart_user_migration_class', ""))->defaultValue(0);
    }
    else {
      $this->addFieldMapping('uid', 'uid')->defaultValue(0);
    }

    $this->addFieldMapping('status', 'status');
    $this->addFieldMapping('start_date', 'created');
    $this->addFieldMapping('due_date', 'next_charge');
    $this->addFieldMapping('end_date', 'end_date')->defaultValue(0);
    $this->addFieldMapping('quantity')->defaultValue(1);
    $this->addFieldMapping('commerce_recurring_fixed_price', 'fee_amount');
    $this->addFieldMapping('commerce_recurring_ref_product')->defaultValue(449);
    $this->addFieldMapping('commerce_recurring_order', 'order_id');
  }

  function prepareRow($row) {
    // The sourceMigration returns order_id and revions_id. We only need the
    // order id, so we call the source migration manually.
    $order = $this->handleSourceMigration($this->orderSourceMigration, $row->order_id);

    // Don't create a transaction if we can't load the order.
    if (empty($order['destid1'])) {
      return FALSE;
    }
    $row->order_id = $order['destid1'];
    
    // cancel date
    $data = unserialize($row->data);
    $row->end_date = isset($data['cancel']) ? $data['cancel'] : 0;
    // status
    $row->status = ($row->status == 0) ? 1 : 0;
  }

  function prepare($subscription, stdClass $row) {
    // The destination line item type is product
    $subscription->type = 'product';

    // Add a base price component.
    $subscription_wrapper = entity_metadata_wrapper('commerce_recurring', $subscription);
    $price = $subscription_wrapper->commerce_recurring_fixed_price;
    $base_price = array(
      'amount' => $price->amount->value(),
      'currency_code' => $price->currency_code->value(),
      'data' => array(),
    );
    $subscription_wrapper->commerce_recurring_fixed_price->data = commerce_price_component_add($base_price, 'base_price', $base_price, TRUE);
    $subscription->data = unserialize($row->data);
  }
}
