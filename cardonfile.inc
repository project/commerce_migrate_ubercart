<?php

/**
 * @file
 *   Commerce Card on File migration.
 */

class CommerceMigrateUbercartCardonFile extends Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Create a map object for tracking the relationships between source rows.
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'rfid' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Authorize.NET card on file id {uc_recurring_users}',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_cardonfile')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_ubercart_get_source_connection();

    $query = $connection->select('uc_recurring_users', 'uru');
    $query->innerJoin('uc_orders', 'uo', 'uru.order_id = uo.order_id');
    $query->condition('uo.order_status', 'in_checkout', '<>');
    $query->fields('uru');
    $query->distinct();
    $query->fields('uo', array('billing_first_name', 'billing_last_name'));
    $query->addField('uo', 'data', 'order_data');
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_cardonfile', 'commerce_cardonfile');

    // Default uid to 0 if we're not mapping it.
    if (variable_get('commerce_migrate_ubercart_user_map_ok', FALSE) && variable_get('commerce_migrate_ubercart_user_migration_class', "") != "") {
      $this->addFieldMapping('uid', 'uid')->sourceMigration(variable_get('commerce_migrate_ubercart_user_migration_class', ""))->defaultValue(0);
    }
    else {
      $this->addFieldMapping('uid', 'uid')->defaultValue(0);
    }

    $this->addFieldMapping('payment_method')->defaultValue('authnet_aim');
    $this->addFieldMapping('instance_id')->defaultValue('authnet_aim|commerce_payment_authnet_aim');
    $this->addFieldMapping('remote_id', 'remote_id');
    $this->addFieldMapping('card_type', 'cc_type')->defaultValue("");
    $this->addFieldMapping('card_name', 'card_name');
    $this->addFieldMapping('card_number', 'cc_last_four');
    $this->addFieldMapping('card_exp_month', 'cc_exp_month');
    $this->addFieldMapping('card_exp_year', 'cc_exp_year');
    $this->addFieldMapping('instance_default')->defaultValue(1);
    $this->addFieldMapping('status')->defaultValue(1);
    $this->addFieldMapping('created', 'created');
    $this->addFieldMapping('changed', 'created');
  }

  function prepareRow($row) {
    // request data from authorize.net
    $fee = uc_recurring_fee_user_load($row->rfid);
    $order = uc_order_load($fee->order_id);
    $refs = array_keys($order->data['cc_txns']['references']);
    $ref_id = end($refs);
    $profile = _uc_authorizenet_cim_profile_get($order, $ref_id);
    // load the name
    $bill_profile = _uc_authorize_cim_xml_billto($order);
    $row->card_name = $bill_profile['firstName'] . ' ' . $bill_profile['lastName'];
    if($profile['resultCode'] == 'Ok'){
      $row->remote_id = $profile['customerProfileId'] . '|' . $profile['customerPaymentProfileId'];
    }
    // get card data from the order data
    if(isset($row->order_data)){
      $order_data = unserialize($row->order_data);
      if(isset($values['cc_txns']) && !empty($values['cc_txns'])){
        $card = reset($values['cc_txns']['references']);
        $row->cc_last_four = $card['card'];
      }
    }
    // get card data from authorize.net
    $customer_profile_id = NULL;
    if($customer_profile_id = authnet_cim_entity_profile_id_load('user', $order->uid)){

    }
    else if($customer_profile_id = authnet_cim_entity_profile_id_load('order', $order->order_id)){

    }
    if($customer_profile_id){
      $record = reset(authnet_cim_entity_customer_payment_profiles_load($customer_profile_id));
      $row->cc_last_four = $record['cc_last_four'];
      $row->cc_type = $record['cc_type'];
      $row->cc_exp_year = $record['cc_exp_year'];
      $row->cc_exp_month = $record['cc_exp_month'];
    }
  }
}
